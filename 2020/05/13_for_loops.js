/**
 * For loops em Javascript
 *
 * 13/05/2020
 * https://gitlab.com/folksilva/code-drops/-/blob/master/2020/05/13_for_loops.js
 */

let meuArray = ['A', 'B', 'C']
let meuObjeto = {a: 0, b: 1, c: 2}

// O clássico
for (let i = 0; i < meuArray.length; i += 1) {
    console.log(meuArray[i])  // A... B... C...
}

// for...in (sobre as propriedades)
for (let i in meuArray) {
    console.log(i)  // 0... 1... 2...
}
for (let i in meuObjeto) {
    console.log(i)  // a... b... c...
}

// for...of (sobre os dados)
for (let i of meuArray) {
    console.log(i)  // A... B... C...
}
for (let i of Object.entries(meuObjeto)) {
    console.log(i)  // ['a', 0]... ['b', 1]... ['c', 2]...
}

// forEach
meuArray.forEach(i => console.log(i))  // 0... 1... 2...
Object.entries(meuObjeto)
    .forEach(i => console.log(i))  // ['a', 0]... ['b', 1]... ['c', 2]...
