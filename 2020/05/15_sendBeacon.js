/**
 * Navigator.sendBeacon
 *
 * Este método envia uma pequena porção de dados de forma assíncrona para um
 * servidor através de HTTP POST, mesmo antes de a página ser fechada.
 * Util para registrar dados de análise, logs e diagnóstico da aplicação.
 *
 * Sintaxe:
 * navigator.sendBeacon(url, data);
 *
 * Parâmetros:
 * - url: A URL que vai receber os dados, pode ser absoluta ou relativa
 * - data: Os dados que serão enviados, pode ser um objeto ArrayBuffer,
 * ArrayBufferView, Blob, DOMString, FormData ou URLSearchParams.
 *
 * Retorno:
 * Se o navegador aceitou os dados para envio retorna true, senão false.
 *
 * Compatibilidade (https://caniuse.com/#feat=beacon):
 * - Desktop: Chrome, Edge, Firefox, Opera e Safari
 * - Mobile: Android webview, Chrome, Firefox, Opera, Safari e Samsung
 */

const url = 'https://jsonplaceholder.typicode.com/posts'
const data = new FormData()
data.append('title', 'foo')
data.append('body', 'bar')
data.append('userId', 1)

const beaconSent = navigator.sendBeacon(url, data)
console.log(beaconSent)  // true
